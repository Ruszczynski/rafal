const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require ('autoprefixer');
const postcss = require ('gulp-postcss');
const cssnano = require ('cssnano');
const purgecss = require('gulp-purgecss');
const webpack = require('webpack');


sass.compiler = require('node-sass');

var webpackConfigPath = './gulpfiles/webpack.config.js';
var webpackConfigPathProd = './gulpfiles/webpackProd.config.js';

//Konwersja SCSS na CSS
gulp.task('sass', function (done) {
  return gulp.src('_src/scss/app_style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      includePaths:['./node_modules']
    }))
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([ autoprefixer() ]))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest('./build/assets/css'))
    .on("end", done);
});

//Uruchamianie serwera lokalnego
gulp.task('server', function(done) {
  browserSync.init({
      server: {
          baseDir: "./build"
      }
    });
    done();
});

//minifikacja
gulp.task('cssmin', (done) => {
  gulp.src('./build/assets/css/app_style.css')
    .pipe(postcss([cssnano]))
    .pipe(gulp.dest('./build/assets/css/'))
    .on('end', done);
});

//PURGECSS
gulp.task('purgecss', () => {
  return gulp
    .src('./build/assets/css/app_style.css')
    .pipe( 
      purgecss({
        content: ['build/**/*.html'],
        whitelist: ['show', 'open']
      })
    )
    .pipe(gulp.dest('build/assets/css/'))
})

//przeładowanie
gulp.task('reload', (done) => {
  browserSync.reload();
  done();
});

//webpack
gulp.task('es6', (done) => {
  return webpack(require(webpackConfigPath), function(err, stats){
    if(err) throw err;
    console.log( stats.toString() );
    done();
  });
});

//webpack produkcyjny
gulp.task('es6prod', (done) => {
  return webpack(require(webpackConfigPathProd), function(err, stats){
    if(err) throw err;
    console.log( stats.toString() );
    done();
  });
});


//nasłuchiwanie, przetwarzanie i przeładowanie
gulp.task('watch', function(done) {
  gulp.watch('./_src/scss/**/*.scss', gulp.series('sass', 'reload'));
  gulp.watch('./_src/js/**/*.js', gulp.series('es6', 'reload'));
  gulp.watch('./build/**/*.html', gulp.series('reload'));
  done();
});

//przypisanie domyślnych funkcji dla GULP
gulp.task('default', gulp.series('sass', 'es6', 'server', 'watch'));


//wersja produkcyjna
gulp.task ('prod', gulp.series('sass', 'cssmin', 'purgecss', 'es6prod'));