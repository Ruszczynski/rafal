export default function(stars, radios){

		stars.on("mouseenter", function(){

				var that = $(this);

				that.prevAll().addBack().addClass("swieca");
		});

		stars.on("mouseleave", function(){

			var that = $(this);

			that.prevAll().addBack().removeClass("swieca");
		});

		stars.on("click", function(){

			var that = $(this),
					index = that.index();

			that.siblings().removeClass("swieca_na_stale");
			that.prevAll().addBack().addClass("swieca_na_stale");

			radios.eq(index).prop("checked", true);


		});


}