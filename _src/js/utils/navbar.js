export default function(){

	var originalAddClassMethod = $.fn.addClass;

	$.fn.addClass = function(){
			var result = originalAddClassMethod.apply( this, arguments );

			$(this).trigger('cssClassChanged');

			return result;
	}

	$(".progress_bar .nav-link").bind('cssClassChanged' , function(e) {
		$(".nav-item").each( function() {
				if( $(this).hasClass("active") == true ) {
						$(this).removeClass("active");
				}
		});

		$(this).removeClass("active").parent().addClass("active");
});

}
