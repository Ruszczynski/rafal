/*tooltipy*/

export default (function(){

	var globalTooltip = null;

	function createTooltip(text, options){
		var tooltip = document.createElement("div");
				tooltip.className = "tooltip";
				tooltip.textContent = text;
				// tooltip.style.width = options.w + "px";

				document.body.appendChild(tooltip);
				
				tooltip.style.left = (options.x + options.w/2) + "px";
				tooltip.style.top = (options.y - tooltip.offsetHeight - 10) + "px";


		globalTooltip = tooltip;

	};

	function findPos (element){
		if (element) {
			var parentPos = findPos(element.offsetParent);
			return [
				parentPos[0] + element.offsetLeft,
				parentPos[1] + element.offsetTop
			];
		} else {
			return [0,0];
		}
	}

	function showTooltip(e){
		var text = e.target.getAttribute("title"),
				options = {
				w: e.target.offsetWidth,
				x: findPos(e.target)[0],
				y: findPos(e.target)[1]
		};

		createTooltip(text, options);

		e.target.removeAttribute("title");

	};

	function hideTooltip(e){
		e.target.setAttribute("title", globalTooltip.textContent);
		globalTooltip.parentNode.removeChild(globalTooltip);
	};

	function assignEvents(elementypobrane, akcja, funkcja){

		for(var i = 0; i < elementypobrane.length; i++){
			elementypobrane[i].addEventListener(akcja, funkcja, false);
		};
	};

	function odpalacz(elementypobrane){
		assignEvents(elementypobrane, 'mouseenter', showTooltip);
		assignEvents(elementypobrane, 'mouseleave', hideTooltip);
	};

	odpalacz(document.querySelectorAll("[title]"));

})();