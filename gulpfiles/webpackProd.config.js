const { resolve } = require("path");
const webpack = require ('webpack');

module.exports = {
    mode : 'production',

    entry: {
        app: "./_src/js/entry.js"
    },

    output: {
        path: resolve(__dirname, "..build/assets/js"),
        filename: "[name].js"
    },

    module: {
        rules: [
            {
            test: /\/js$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader",
                options: {
                    presets: ["@babel/preset-env"]
                    
                }
            }
        }
    ]
},

    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'widow.jQuery': 'jquery'
        })
    ]

};