const { resolve } = require("path");
const webpack = require ('webpack');

module.exports = {
    mode : 'development',

    entry: {
        app: "./_src/js/entry.js"
    },

    output: {
        path: resolve(__dirname, "../build/assets/js"),
        filename: "[name].js"
    },

    module: {
        rules: [
            {
            test: /\/js$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader"
            }
         }
    ]
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'widow.jQuery': 'jquery'
        })
    ]

};